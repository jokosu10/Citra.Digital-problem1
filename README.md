### Cara menjalankan program
=============================

**Spesifikasi minimal software untuk menjalankan program**
* php-cli minimal versi **7.2**.
* terminal / command prompt.

=============================

**Cara menjalankan program**
1. Buka terminal.
2. Masuk ke directory.
3. Jalankan program dengan mengetikan `php encode.php` pada terminal.
