<?php

    function encodeProcess($string)
    {
        $hurufHidup = ['a','i','u','e','o'];
        $arrayString = str_split($string);
        foreach(array_values($arrayString) as $i => $value) {
            if (in_array($value, $hurufHidup)) {
                echo strtoupper($value);
            } else {
                echo $i."+";
            }
        }
        echo "\n";
    }

    echo "Masukkan string anda : ";
    $inputString = fopen("php://stdin","r");
    $string = trim(fgets($inputString));
    $teksB = 'b';
    $teksP = 'p';
    $posisiB = strpos($string, $teksB);
    $posisiP = strpos($string, $teksP);

    // cek bila ada huruf p & b
    if ($posisiP === false && $posisiB === false || $posisiB === false && $posisiP === false || $posisiP !== false && $posisiB === false || $posisiB !== false && $posisiP === false)  {
        $string = $string;
    } else {
        // $tmp = $posisiB;
        // $posisiB = $posisiP;
        // $posisiB = 'p';
        // $posisiP = $tmp;
        // $posisiP = 'b';

        $string = substr_replace($string, $teksB, $posisiP, 1);
        $string = substr_replace($string, $teksP, $posisiB, 1);
    }

    $result = encodeProcess($string);
    echo $result;

?>
